# IXS LDAP Integration

### Set up
#### Build Jar  
To generate the jar, check out this repository and run:

```
mvn clean install -DskipTests
```

#### Copy jar
Search for ```console-common-ixs-ldap-7.8.4.jar``` in ```target``` dir.
Copy the jar to ```BONITA_HOME/server/webapps/bonita/WEB-INF/lib/``` dir.

#### Configuration
Add the following to ```authenticationManager-config.properties```

```
auth.AuthenticationManager=org.bonitasoft.console.common.ixs.ldap.LdapAuthenticationManagerImpl
ldap.host=pdc.ixs.lan
ldap.port=389
ldap.baseDistinguishedName=ou=External,ou=Employees,dc=ixs,dc=lan
```

#### Restart bonita
Stop and start bonita.

#### Login
Goto Bonita login page and use the following for username/password:
```
Username: bonita-dev
Password: Welcome01!
```

#### Catalina logs
If ldap authentication fails, ```bonita.log``` will contain an error like this:
```
SEVERE: org.bonitasoft.console.common.ixs.ldap.LdapAuthenticationManagerImpl [LDAP: error code 49 - 80090308: LdapErr: DSID-0C0903A9, comment: AcceptSecurityContext error, data 52e, v1db1]
```

If ldap authentication was successful, it will try to forward request to LoginDatastore which may connect to
local database. Local database should have the same username/password for login
to be successful.

