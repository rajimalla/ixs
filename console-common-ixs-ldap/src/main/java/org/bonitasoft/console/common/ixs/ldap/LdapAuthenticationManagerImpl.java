package org.bonitasoft.console.common.ixs.ldap;

import org.apache.commons.lang3.StringUtils;
import org.bonitasoft.console.common.server.auth.AuthenticationFailedException;
import org.bonitasoft.console.common.server.auth.AuthenticationManager;
import org.bonitasoft.console.common.server.auth.ConsumerNotFoundException;
import org.bonitasoft.console.common.server.login.HttpServletRequestAccessor;
import org.bonitasoft.console.common.server.login.TenantIdAccessorFactory;
import org.bonitasoft.console.common.server.login.credentials.Credentials;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.ServletException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.bonitasoft.console.common.server.utils.TenantsManagementUtils.getDefaultTenantId;

public class LdapAuthenticationManagerImpl implements AuthenticationManager {

    private static final Logger LOGGER = Logger.getLogger(LdapAuthenticationManagerImpl.class.getName());

    @Override
    public String getLoginPageURL(HttpServletRequestAccessor request, String redirectURL)
            throws ConsumerNotFoundException, ServletException {

        final StringBuilder url = new StringBuilder();
        String context = request.asHttpServletRequest().getContextPath();
        final String servletPath = request.asHttpServletRequest().getServletPath();
        if (StringUtils.isNotBlank(servletPath) && servletPath.startsWith("/mobile")) {
            context += "/mobile";
        }
        url.append(context).append(AuthenticationManager.LOGIN_PAGE).append("?");
        long tenantId = getTenantId(request);
        if (tenantId != getDefaultTenantId()) {
            url.append(AuthenticationManager.TENANT).append("=").append(tenantId).append("&");
        }
        url.append(AuthenticationManager.REDIRECT_URL).append("=").append(redirectURL);
        return url.toString();

    }

    private long getTenantId(HttpServletRequestAccessor request) throws ServletException {
        return TenantIdAccessorFactory.getTenantIdAccessor(request).getTenantIdFromRequestOrCookie();
    }

    @Override
    public Map<String, Serializable> authenticate(HttpServletRequestAccessor request, Credentials credentials)
            throws AuthenticationFailedException, ServletException {

        // fetch the tenant id
        long tenantId = getTenantId(request);

        // fetch ldap confiuration
        String ldapServerHost = LdapAuthenticationManagerProperties.getProperties(tenantId).getLdapServerHost();
        String ldapServerPort = LdapAuthenticationManagerProperties.getProperties(tenantId).getLdapServerPort();
        String ldapBaseDN = LdapAuthenticationManagerProperties.getProperties(tenantId).getLdapBaseDistinguishedName();

        String providerUrl = String.format("ldap://%s:%s", ldapServerHost, ldapServerPort);
        String securityPrincipal = String.format("cn=%s,%s", credentials.getName(), ldapBaseDN);

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, providerUrl);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, securityPrincipal); // specify the username
        env.put(Context.SECURITY_CREDENTIALS, credentials.getPassword());

        try {
            DirContext context = new InitialDirContext(env);

            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Successfully authenticated against LDAP server.");
            }

            return Collections.emptyMap();
        } catch (NamingException e) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, e.getMessage());
            }
            throw new AuthenticationFailedException(e.getMessage(), e);
        }
    }

    @Override
    public String getLogoutPageURL(HttpServletRequestAccessor request, String redirectURL)
            throws ConsumerNotFoundException, ServletException {
        return null;
    }
}
