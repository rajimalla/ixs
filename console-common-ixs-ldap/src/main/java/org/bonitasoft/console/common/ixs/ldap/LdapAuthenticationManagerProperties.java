package org.bonitasoft.console.common.ixs.ldap;

import org.bonitasoft.console.common.server.auth.AuthenticationManagerProperties;

import java.util.logging.Logger;

public class LdapAuthenticationManagerProperties extends AuthenticationManagerProperties {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(LdapAuthenticationManagerProperties.class.getName());

    /**
     * Configuration of Ldap Server Host
     */
    protected static final String LDAP_SERVER_HOST = "ldap.host";

    /**
     * Configuration of Ldap Server Port
     */
    protected static final String LDAP_SERVER_PORT = "ldap.port";

    /**
     * Configuration of Ldap Base DN
     */
    protected static final String LDAP_BASE_DN = "ldap.baseDistinguishedName";


    /**
     * properties
     *
     * @param tenantId
     */
    public LdapAuthenticationManagerProperties(final long tenantId) {
        super(tenantId);
    }

    public static LdapAuthenticationManagerProperties getProperties(long tenantId) {
        return new LdapAuthenticationManagerProperties(tenantId);
    }

    /**
     *
     * @return get ldap server host
     */
    public String getLdapServerHost() {
        return getProperty(LDAP_SERVER_HOST);
    }

    /**
     *
     * @return get ldap server port
     */
    public String getLdapServerPort() {
        return getProperty(LDAP_SERVER_PORT);
    }

    /**
     *
     * @return get ldap server Base DN.
     */
    public String getLdapBaseDistinguishedName() {
        return getProperty(LDAP_BASE_DN);
    }

}
